/*  1. document.createElement(tag)
    Створює новий елемент з заданим тегом:

    let div = document.createElement('div');

    Щоб div показався нам потрібно вставити його десь на сторінці в document. 
    Наприклад, в елемент <body> який можна отримати звернувшись до document.body.

    Для цього існує спеціальний метод append: document.body.append(div).

    2. insertAdjacentHTML()розбирає вказівок текст як HTML або XML і встановлює
    отримані вузли у дереві DOM у вказаній позиції. Ця функція не переписує
    наявні елементи, що передбачає додаткову серіалізацію, і тому працює швидше, ніж
    маніпуляції з innerHTML.

    Синтаксис

    targetElement.insertAdjacentHTML(position, text);

    Параметри
    position
    DOMString- визначає позицію доданого елемента щодо елемента, викликаного методом.
    Потрібно відповідати одному з наступних значень (чутливо до регістру):

    'beforebegin': до самого element(до відкриваючого тега).
    'afterbegin': відразу після тега element, що відкриває (перед першим нащадком).
    'beforeend': відразу перед закриваючим тегом element(після останнього нащадка).
    'afterend': після element(після закриває тега).
    text
    Рядок, який буде проаналізований як HTML або XML і вставлений у DOM-дерево документа.

    3. Метод Element.remove()видаляє елемент з DOM-дерева, в якому він знаходиться.
    
*/

const cities = ["Kiev", "Borispol", "Irpin", "Kharkiv", "Odessa", "Lviv", "Dnieper"];

function citiesOfUkraine(cities, parent = document.body) {
const ulEL = document.createElement('ul');
cities.forEach((value) =>{
    let li = document.createElement('li');
    li.innerText = value;
    ulEL.append(li);
});
parent.append(ulEL)
};
citiesOfUkraine(cities)